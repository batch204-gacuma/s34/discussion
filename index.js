 const express = require("express");

 const app = express();

 app.use(express.json());

 const port = 3000;

 app.get("/", (req, res) => {
 	console.log(req.body)
 	res.send("Hello World")
 })
app.post("/hello", (req, res) =>{
	console.log(req.body)
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})
let users = [];
app.post("/signup", (req, res) =>{
	console.log(req.body);
	if(req.body.username !== "" && req.body.password !== "" && req.body.username !== undefined && req.body.password !== undefined){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`)
	}else{
		res.send(`Invalid!`)
	}
})
app.get("/users", (req, res)=>{
	res.send(users);
})
app.put("/change-password", (req, res)=>{
	let message;
	for(let i=0; i < users.length; i++){

		if(users[i].username === req.body.username){
			message = `User ${req.body.username}'s password has been updated.`
			break;
		}else{
			message = "User does not exist!";
		}
	}
})
app.delete("/delete-user", (req, res) => {

		let message;

		for (let i = 0; i < users.length; i++) {
			if (users[i].username === req.body.username) {

				users.splice(users[i], 1);

				message = `User ${req.body.username} has been deleted.`;
				break;
			}
			else {
				message = `User does not exist!`;
			}
		}

		// Sends a response back to the client/Postman once the password has been updated or if a user is not found.
		res.send(message);
	});


 app.listen(port, () => console.log(`Server is running at port: ${port}`));